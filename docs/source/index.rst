.. csv-jsonl documentation master file, created by
   sphinx-quickstart on Thu Apr  7 14:56:15 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

csv-jsonl
=========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: csv_jsonl
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
